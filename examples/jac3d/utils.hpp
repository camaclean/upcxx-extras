#ifdef _UTILS_HPP
#else
#define _UTILS_HPP

#include <stdio.h>
#include <iostream>

// CUDA includes
#include <cuda_runtime.h>

using namespace std;

void checkCUDAError(const char *msg);
int lastCudaErr(int rank, const char* mesg=NULL);
int ReportDevice();
void printMesh(int *n, double *U, const int rank);

//
//  Assert style handler function and wrapper macro
//  Wrap each API call with the gpuErrchk macro, which will process return
//  status of the API call it wraps, for example:

// 	gpuErrchk( cudaMalloc(&a_d, size*sizeof(int)) );

// If there is an error in a call, a message describing the error, file and
// line in the code where the error occurred will be emitted to stderr and
// the application will exit.
// Can't handle kernel calls; see this URL for details of how to do so
// https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api#14038590

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line) {
   if (code != cudaSuccess) {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
              line);
      abort();
   }
}
#endif
