#include "upcxx-extras/padded_cuda_allocator.hpp"

#ifdef _CONFIG_H
#else
#define _CONFIG_H
// define precision 
#define POISSON

static const double pi = 3.14159265358979323846;

#endif
