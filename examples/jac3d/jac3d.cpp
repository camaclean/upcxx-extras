#include <stdio.h>
#include <iostream>
#include <chrono>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include "config.hpp"
#include "utils.hpp"
#include "gpuFuns.hpp"
#include "ghostRegion.hpp"

#if UPCXX_VERSION < 20190300
#error This test requires UPC++ 2019.3.0 or newer (cuda support)
#endif

using namespace std;

void cmdLine(int argc,char **argv, int rank, int numprocs, int& Nx, int& Ny,
        int& Nz, int& nIters, int& px, int& py, int& pz, int& use_pitched);
double*** Alloc3D(int nx, int ny, int nz, char *mesg);
double  l2norm(double* U, double* B,const int nx, const int ny, const int nz,
        const int Nx, const int Ny, const int Nz);
double max_norm(double* U, double* B,const int nx, const int ny, const int nz,
        const int Nx, const int Ny, const int Nz);
double GFlops(const double time, const int Nx,const int Ny, const int Nz,
        const int iters);
void printTOD(const char *mesg);
void OrderlyOutput(ostringstream& s1);
void initMesh(int *n, int *p, int *r, double *U, double *Un, double *B);
void PrintUsage(const char *const program);

// A sparse table with expected results for various problem configurations
typedef struct _Jacobi3DSolution {
    int Nx;
    int Ny;
    int Nz;
    int iters;
    double expected_resid;
} Jacobi3DSolution;

static Jacobi3DSolution expectedSolutions[] = {
    {128, 128, 128, 1000, 1.390334e-03},
    {128, 128, 128, 2000, 7.833854e-04},
    {256, 256, 256, 1000, 1.026033e-03},
    {256, 256, 256, 2000, 6.022070e-04},
    {512, 512, 512, 1000, 7.333697e-04},
    {512, 512, 512, 2000, 4.348092e-04},
    {768, 768, 768, 1000, 6.000679e-04},
    {768, 768, 768, 2000, 3.564220e-04},
    {1024, 1024, 1024, 1000, 5.200858e-04},
    {1024, 1024, 1024, 2000, 3.091104e-04}
};

extern void GPU_jac3d_driver(dim3& dimGrid, dim3& dimBlock, int nx,
        int ny, int nz, double *d_U, double *d_Un, double *d_B,
        size_t pitch_floats, size_t pitch_bytes);

int lastCudaErr(int rank, const char *mesg) {
    cudaError_t c_err = cudaGetLastError();
    ostringstream sl;

    int gerr = upcxx::reduce_all((int)c_err, upcxx::op_fast_bit_or).wait();

    if (!gerr){
        if (!rank){
            if (mesg != NULL) sl << mesg << ": ";
            sl << "No CUDA Errors!\n";
        }
    } else {
        if (mesg != NULL) sl << mesg << ": ";
        sl << "CUDA error from rank " << rank << ": " <<
            cudaGetErrorString(c_err) << endl;
    }
    OrderlyOutput(sl);
    return gerr;
}

static void checkErr(int Nx, int Ny, int Nz, int nIters, double resid) {
    for (size_t i = 0; i < sizeof(expectedSolutions) / sizeof(expectedSolutions[0]); i++) {
        Jacobi3DSolution *sol = &expectedSolutions[i];
        if (sol->Nx == Nx && sol->Ny == Ny && sol->Nz == Nz &&
                sol->iters == nIters) {
            double perc_err = fabs((resid - sol->expected_resid) /
                sol->expected_resid);
            printf("Expected residual = %e, computed residual = %e, "
                    "absolute error = %e, percent error = %e\n",
                    sol->expected_resid, resid,
                    fabs(resid - sol->expected_resid), perc_err);
            if (perc_err > 0.01) { // 1% error
                printf("FAIL\n");
            } else {
                printf("SUCCESS\n");
            }
            return;
        }
    }

    printf("WARNING: Unable to validate residual, unsupported problem size.\n");
    printf("SUCCESS\n");
}

inline double lambda(int n) {
    double k[3] = {1., 1., 1.};
    return n * n * (2 * (cos(k[0]*pi/n) + cos(k[1]*pi/n) + cos(k[2]*pi/n)) - 6);
}

int main(int argc,char **argv)
{
    double *d_B=NULL;
    double *U=NULL, *Un=NULL, *B;
   // Configuration variables
    int Nx,Ny,Nz, nIters;
    int px, py, pz;
    int nx, ny, nz;
    int numprocs, rank;
    int rankx, ranky, rankz;
    int use_pitched;

    upcxx::init();
    numprocs = upcxx::rank_n();
    rank = upcxx::rank_me();

    cmdLine(argc,argv,rank,numprocs,Nx,Ny,Nz,nIters,px,py,pz,use_pitched);

    nx = Nx/px, ny = Ny/py, nz = Nz/pz;

    // CUDA kernel configuration
    dim3 dimBlock(16, 8, 8);
    dim3 dimGrid(((nx+1) + dimBlock.x - 1) / dimBlock.x,
                 ((ny+1) + dimBlock.y - 1) / dimBlock.y,
                 ((nz+1) + dimBlock.z - 1) / dimBlock.z);

    // UPC++ rank layout/dimensions
    rankz = rank / (px*py);
    ranky = (rank % (px*py)) / px;
    rankx = (rank % (px*py)) % px;

    // Dimensionality of the local grid
    int nxyz[3]; nxyz[0] = nx; nxyz[1] = ny; nxyz[2] = nz;
    // Dimensionality of 3D rank grid
    int pxyz[3]; pxyz[0] = px; pxyz[1] = py; pxyz[2] = pz;
    // My 3D coordinate in the 3D rank grid
    int rxyz[3]; rxyz[0] = rankx; rxyz[1] = ranky; rxyz[2] = rankz;

    // Allocate and clear the timers
    // These store the time by iteration.
    double *tI = new double[nIters];
    if ( !tI ) fprintf(stderr, "Unable to allocate timer array on rank %d\n",rank);
    for (int i=0; i<nIters; i++) tI[i] = 0.0;

    if (!rank){
        cout << "\n\n7-Point Point Jacobi with [Nx Ny Nz] = [";
        cout << Nx << " " << Ny << " " << Nz << "]\n";

#ifdef POISSON
        cout << "Solving Poisson's Equation\n";
#else
        cout << "Solving Laplace's Equation\n";
#endif
        cout << "Using DOUBLE Precision\n";
        cout << "iterations:  " << nIters << endl;
        if (use_pitched) {
            cout  << "using pitched GPU allocations" << endl << endl;
        } else {
            cout  << "not using pitched GPU allocations" << endl << endl;
        }
    }
    // Report device attributes
    if (!rank)
        ReportDevice();

    upcxx::barrier();

    ghostRegion<double> *Ghosts = new ghostRegion<double>(nxyz, pxyz, rxyz,
            dimGrid, dimBlock);

    size_t required_dev_mem = Ghosts->getRequiredDevMem() +
        2 * estimate_pitched_memory_usage<double>(nx+2, (ny+2)*(nz+2));
    const size_t mb = 1024 * 1024;
    required_dev_mem = required_dev_mem + (mb - (required_dev_mem % mb));

    upcxx::cuda_device gpu_device(0);
    upcxx::extras::padded_cuda_allocator gpu_alloc(gpu_device,
            required_dev_mem);

    gpuErrchk(cudaMallocHost((void**)&U, sizeof(double)*(nx+2)*(ny+2)*(nz+2)));
    gpuErrchk(cudaMallocHost((void**)&Un, sizeof(double)*(nx+2)*(ny+2)*(nz+2)));

    size_t pitch_bytes;
    upcxx::global_ptr<double, upcxx::memory_kind::cuda_device> d_U, d_Un;
    if (use_pitched) {
        d_U = gpu_alloc.allocate_pitched<double>((nx + 2), (ny + 2) * (nz + 2),
                    pitch_bytes);
        assert(d_U);
        d_Un = gpu_alloc.allocate_pitched<double>((nx + 2), (ny + 2) * (nz + 2),
                    pitch_bytes);
        assert(d_Un);
    } else {
        d_U = gpu_alloc.allocate<double>((nx + 2) * (ny + 2) * (nz + 2));
        assert(d_U);
        d_Un = gpu_alloc.allocate<double>((nx + 2) * (ny + 2) * (nz + 2));
        assert(d_Un);
        pitch_bytes = (nx + 2) * sizeof(double);
    }

    assert(pitch_bytes % sizeof(double) == 0);
    size_t pitch_floats = pitch_bytes / sizeof(double);
    if (rank == 0) {
        printf("pitch bytes = %lu, pitch floats = %lu\n", pitch_bytes,
                pitch_floats);
    }

#ifdef POISSON
    gpuErrchk(cudaMallocHost((void**)&B,sizeof(double)*(nx+2)*(ny+2)*(nz+2)));
    // Doesn't need to use allocate_pitched because d_B is never communicated.
    size_t d_B_pitch_bytes;
    if (use_pitched) {
        gpuErrchk(cudaMallocPitch((void **)&d_B, &d_B_pitch_bytes,
                    sizeof(double)*(nx+2), (ny+2)*(nz+2)));
    } else {
        gpuErrchk(cudaMalloc((void **)&d_B,
                    sizeof(double)*(nx+2)*(ny+2)*(nz+2)));
        d_B_pitch_bytes = (nx+2)*sizeof(double);
    }
    assert(d_B_pitch_bytes == pitch_bytes);
#endif

    initMesh(nxyz, pxyz, rxyz, U, Un, B);

#ifdef PRINTMESH
    printMesh(nxyz,U, rank);
#endif

    // Copy down
    gpuErrchk(cudaMemcpy2D(gpu_alloc.local(d_U), pitch_bytes,
                U, sizeof(double) * (nx+2),
                sizeof(double) * (nx+2),
                (ny+2) * (nz+2),
                cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy2D(gpu_alloc.local(d_Un), pitch_bytes,
                U, sizeof(double) * (nx+2),
                sizeof(double) * (nx+2),
                (ny+2) * (nz+2),
                cudaMemcpyHostToDevice));
#ifdef POISSON
    gpuErrchk(cudaMemcpy2D(d_B, pitch_bytes,
                B, sizeof(double) * (nx+2),
                sizeof(double) * (nx+2),
                (ny+2) * (nz+2),
                cudaMemcpyHostToDevice));
#endif

    gpuErrchk(cudaDeviceSynchronize());

    // Set up the execution configuration
    Ghosts->allocBuffs(gpu_alloc, d_U, d_Un);

    upcxx::barrier();
    if (!rank){
        cout << "DimGrid  = " << dimGrid.x << " " << dimGrid.y << " " << dimGrid.z << endl;
        cout << "DimBlock  = " << dimBlock.x << " " << dimBlock.y << " " << dimBlock.z << endl;
        cout << "px = " << px << "   py = " << py << "   pz = " << pz << endl;
        cout << "nx = " << nx << "   ny = " << ny << "   nz = " << nz << endl;
        printTOD("Run begins");
    }

    std::chrono::steady_clock::time_point startIter =
        std::chrono::steady_clock::now();

    gpuErrchk(cudaDeviceSynchronize());

    upcxx::barrier();

    for (int i = 1; i <= nIters; i++) {

        GPU_jac3d_driver(dimGrid, dimBlock, nx, ny, nz,
                gpu_alloc.local(d_U), gpu_alloc.local(d_Un), d_B,
                pitch_floats, pitch_bytes);
        gpuErrchk(cudaDeviceSynchronize());
        gpuErrchk(cudaGetLastError());

        Ghosts->fillGPU(d_Un, gpu_alloc, pitch_bytes, pitch_floats);
        gpuErrchk(cudaDeviceSynchronize()); 
        gpuErrchk(cudaGetLastError());

        std::swap(d_U, d_Un);
    }

    upcxx::barrier();

    double timeIter = std::chrono::duration<double>(
            std::chrono::steady_clock::now() - startIter).count();

    std::chrono::steady_clock::time_point startCopyback =
        std::chrono::steady_clock::now();

    gpuErrchk(cudaMemcpy2D(U, sizeof(double)*(nx+2),
                gpu_alloc.local(d_U),
                pitch_bytes,
                sizeof(double)*(nx+2),
                (ny+2)*(nz+2),
                cudaMemcpyDeviceToHost));
    double t_copyBack = std::chrono::duration<double>(
            std::chrono::steady_clock::now() - startCopyback).count();

    // *** Residual for the Laplace equation not yet implemented

    double l2resid = l2norm(U, B, nx+2, ny+2, nz+2, Nx, Ny, Nz);

    if (rank==0) {
        // ***  Copyback time measurement turned off. Not sure it is accurate.
        //      printf("\nCopy Unew to host: %e (s) \n",t_copyback);
        printf("\nTotal wall-clock time (sec) : %e\n", timeIter);
        printf("Per iteration time    (sec) : %e\n",timeIter/(double)nIters);
        printf("GFLOPS                      : %f\n" , GFlops(timeIter,Nx,Ny,Nz,nIters));
        printf("L2Norm of the Error         : %f\n" , l2resid);
        printf("     Nx    Ny     Nz   Px  Py  Pz     It       Time    Gflops   PREC  P/L   Err\n"); // Abbreviated output for data scraping
        printf("#> ");
        printf("%4d ",Nx);
        printf(" %4d  ",Ny);
        printf(" %4d  ",Nz);
        printf("%3d",px);
        printf(" %3d",py);
        printf(" %3d ",pz);
        printf("  %4d ",nIters);
        printf("  %9.2e",timeIter);
        printf("  %7.2e ", GFlops(timeIter,Nx,Ny,Nz,nIters));
        printf("  DP ");
#ifdef POISSON
        printf("   P");
#else
        printf("   L");
#endif
        printf("  %e\n", l2resid);
        checkErr(Nx, Ny, Nz, nIters, l2resid);
        printTOD("\nRun Completes");
    }

    lastCudaErr(rank, NULL);

    // Release GPU and CPU memory
    gpu_alloc.deallocate(d_U);
    gpu_alloc.deallocate(d_Un);

    Ghosts->deallocBuffs(gpu_alloc);

#ifdef POISSON
    cudaFree(d_B);
#endif

    gpu_device.destroy();

    delete [] tI;
    upcxx::finalize();
    return EXIT_SUCCESS;
}
