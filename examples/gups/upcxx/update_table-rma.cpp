// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//
//  update_table -- multi-threaded update_table for 2 or more threads
//  RMA Implementation
//  Uses a non-scalable solution to solving the bootstrapping problem
//

//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <chrono>
#include <upcxx/upcxx.hpp>

#include "block.cpp"
#include "startr.cpp"
#include "guppie.hpp"
#include "guppie-upcxx.hpp"

using namespace std;
using namespace upcxx;

const char *alg_name = "RMA";
int alg_error_free = 0; // non-atomic explicit split-phase batching of reads and writes

// Main table
global_ptr<uint64> table;

void update_table(int myrank, int nranks, int Vlen,
                  int   ltabsize,
		  const int64 tabsize, const int64 nupdate, int freq, double thresh)
{
    int64 i, j;
    uint64 t1[MAX_VLEN];
    global_ptr<uint64> dest_gptr[MAX_VLEN];
    assert(Vlen <= MAX_VLEN);

    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);
//    ostringstream sl;
//    sl << "[" << myrank << "]: " <<  i0 << ", " << i1 << endl;
//    OrderlyOutput(sl);

    table = new_array<uint64>(locTabSize);
    assert(!table.is_null());
    upcxx::dist_object<global_ptr<uint64>> PTab(table);

    // We really want VLA here, but that's a GNU extension.
    // this is a hack to workaround the lack of VLA in ISO C++
    // without introducing an extra level of indirection
    #ifndef MAX_RANKS
    #define MAX_RANKS (64*1024)
    #endif
    if (nranks > MAX_RANKS) {
      if (!myrank) 
        cerr << "ERROR: nranks > " << MAX_RANKS << ". Please recompile with CXXFLAGS=-DMAX_RANKS=" << nranks << endl;
      upcxx::finalize();
      exit(-1);
    }
    global_ptr<uint64> ldirectory[MAX_RANKS];

    // Probably use an offset to make this more scalable
    for (i=0; i<nranks; i++)
        ldirectory[i] = PTab.fetch(i).wait();
    int n_local = 0;

    // If all entries in the directory satisfy is_local()
    // We can use a much more efficient code variant that
    // uses conventional pointers and memory accesses
    // in lieu of global pointers and memory
    for (i=0; i<nranks; i++)
        n_local += (ldirectory[i].is_local());
    bool isSharedMem = (n_local == nranks);

    upcxx::barrier();

    // Begin init timing here
    auto T0_i = chrono::steady_clock::now();

    assert(table.is_local());
    uint64 *pt = table.local();
    // Initialize main table
    for(i = i0, j=0; i <= i1; i++, j++)   // converts to local view index
	pt[j] = i;

    upcxx::barrier();

    // End init timing here
    auto T1_i = chrono::steady_clock::now();
    chrono::duration<double> t_walltime_i = T1_i-T0_i;
    double walltime_i = t_walltime_i.count();

    int64 start, stop, size;
    upcxx::barrier();

    // Begin update timing here
    auto T0 = chrono::steady_clock::now();
    
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;


    const int64 tabszm1 = tabsize-1;
    const int64 ltabszm1 = locTabSize-1;
    const int64 iTabSz = log2(locTabSize);
    // Single node optimization, straight C++ 
    if (isSharedMem){
        uint64* p_ldir[MAX_RANKS];
        // Create C pointer version of the global_pointers
        for (int k=0; k<nranks; k++)
            p_ldir[k] =  ldirectory[k].local();
        uint64 ran = startr(start);
        for (int k =0; k<size; k++){
            ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
            const int64 indxG = ran & tabszm1;   // Truncate to global table size
            const uint64 offs = indxG & ltabszm1;     // Determine offset;
            const uint64 targ =  indxG >> iTabSz;     // Determine target;
            *(p_ldir[targ]+offs) ^= ran;
        }
    }
    else{
        uint64 ran[MAX_VLEN];             // Current random numbers
        uint64 rans = startr(start);

        for (i=0; i<nbatch; i++) {
            promise<> rgets_done;
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;   // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset;
                const uint64 targ =  indxG >> iTabSz;           // Determine target;
                global_ptr<uint64> dest = ldirectory[targ] + offs;  // compute gptr to table entry
                dest_gptr[j]  = dest;                              // store it for subsequent loops
                if (dest.is_local()){
                    *dest.local() ^= rans;
                }
                else{
                    ran[j] = rans;
                    // Uses simple synchronization
                    // rget the remote values
                    rget(dest, t1+j, 1,
                        upcxx::operation_cx::as_promise(rgets_done));
                }
            }
            rgets_done.finalize().wait();
            promise<> rputs_done;
            for (j=0; j<Vlen; j++){
                global_ptr<uint64> const dest = dest_gptr[j];
                if (!dest.is_local()){
                    uint64 t1s = t1[j];
                    t1s ^= ran[j];
                    rput(t1s, dest, upcxx::operation_cx::as_promise(rputs_done));
                }
            }
            rputs_done.finalize().wait();
        }
    }

    // This barrier ensures quiescence ***
    upcxx::barrier();

    // End update timed section
    auto T1 = chrono::steady_clock::now();
    chrono::duration<double> t_walltime = T1-T0;
    double walltime = t_walltime.count();

    /* Verification of results (in parallel).  */
    uint64 temp = 0x1;
    for (i=0; i<nupdate; i++) {
      temp = (temp << 1) ^ (((int64) temp < 0) ? POLY : 0);
      const uint64 offs = temp & tabszm1;
      if ((offs >= (uint64_t)i0) && (offs <= (uint64_t)i1))
          pt[temp & ltabszm1] ^= temp;
    }
	
    int64 nerrors;
    for (i = i0, j=0, nerrors = 0; i <= i1; i++, j++)
        nerrors += (pt[j] != (uint64_t)i);

//    uint64 totalErrors = allreduce(nerrors, std::plus<uint64>()).wait();
    uint64 totalErrors = reduce_all(nerrors, upcxx::op_fast_add).wait();
	
    reportResults(walltime_i, walltime, nupdate, ltabsize, totalErrors,thresh,Vlen);
    delete_array(table);
    return;
}
