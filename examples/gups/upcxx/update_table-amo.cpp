// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//
//  update_table -- multi-threaded update_table for 2 or more threads
//  AMO Implementation
//  Uses a non-scalable solution to solving the bootstrapping problem
//

//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <chrono>
#include <upcxx/upcxx.hpp>

#include "block.cpp"
#include "startr.cpp"
#include "guppie.hpp"
#include "guppie-upcxx.hpp"

#if UPCXX_VERSION < 20180903
#error The AMO version of this test requires UPC++ 2018.9.3 or newer (issue #177)
#endif

using namespace std;
using namespace upcxx;

#if PIPELINE
const char *alg_name = "AMO-PIPE";
#else
const char *alg_name = "AMO";
#endif
int alg_error_free = 1; // updates are atomic

// Main table
global_ptr<uint64> table;

void update_table(int myrank, int nranks, int Vlen,
                  int   ltabsize,
		  const int64 tabsize, const int64 nupdate, int freq, double thresh)
{
    int64 i, j;
    uint64 *t1 = new uint64[Vlen];

    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);
    const int64 iTabSz = log2(locTabSize);
//    ostringstream sl;
//    sl << "[" << myrank << "]: " <<  i0 << ", " << i1 << endl;
//    OrderlyOutput(sl);

    table = new_array<uint64>(locTabSize);
    assert(!table.is_null());
    upcxx::dist_object<global_ptr<uint64>> PTab(table);

    global_ptr<uint64> *ldirectory = new global_ptr<uint64>[nranks];
    assert(ldirectory);

    // Probably use an offset to make this more scalable
    for (i=0; i<nranks; i++)
        ldirectory[i] = PTab.fetch(i).wait();

    upcxx::barrier();

    // Begin init timing here
    auto T0_i = chrono::steady_clock::now();

    assert(table.is_local());
    uint64 *pt = table.local();
    // Initialize main table
    for(i = i0, j=0; i <= i1; i++, j++)   // converts to local view index
	pt[j] = i;

    upcxx::barrier();

    // End init timing here
    auto T1_i = chrono::steady_clock::now();
    chrono::duration<double> t_walltime_i = T1_i-T0_i;
    double walltime_i = t_walltime_i.count();
    upcxx::atomic_domain<uint64_t> ad_ui64({upcxx::atomic_op::bit_xor});

    int64 start, stop, size;
    upcxx::barrier();

    // Begin update timing here
    auto T0 = chrono::steady_clock::now();
    
    Block(myrank, nranks, nupdate, start, stop, size);

    const int64 nbatch = size/Vlen;
    uint64 ran = startr(start);
    const int64 tabszm1 = tabsize-1;
    const int64 ltabszm1 = locTabSize-1;

  #if PIPELINE
    assert(Vlen % 2 == 0);
    const int Vlen_pipe = Vlen / 2;
    const int nbatch_pipe = nbatch * 2;
    assert(Vlen_pipe * nbatch_pipe == size);
    // * Use two promises with VLEN=512 and leap-frogged them
    promise<> amos_done[2];
    future<> amo_f[2] = { make_future(), make_future() };
    int phase = 0;
    for (i=0; i<nbatch_pipe; i++) {
        amos_done[phase] = promise<>();
	for (j=0; j<Vlen_pipe; j++) {
	    ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
            const int64 indxG = ran & tabszm1;  // Truncate to global table size
            const uint64 offs = indxG & ltabszm1;     // Determine offset;
            const uint64 targ =  indxG >> iTabSz;     // Determine target;

            // update the table
            ad_ui64.bit_xor(ldirectory[targ]+offs, ran, memory_order_relaxed, upcxx::operation_cx::as_promise(amos_done[phase]));
        }
        amo_f[phase] = amos_done[phase].finalize();
        phase = !phase;
        amo_f[phase].wait();
    }
    amo_f[!phase].wait();
  #else
    for (i=0; i<nbatch; i++) {
        promise<> amos_done;
	for (j=0; j<Vlen; j++) {
	    ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
            const int64 indxG = ran & tabszm1;  // Truncate to global table size
            const uint64 offs = indxG & ltabszm1;     // Determine offset;
            const uint64 targ =  indxG >> iTabSz;     // Determine target;

            // update the table
            ad_ui64.bit_xor(ldirectory[targ]+offs, ran, memory_order_relaxed, upcxx::operation_cx::as_promise(amos_done));
        }
        amos_done.finalize().wait();
    }
  #endif

    // This barrier ensures quiescence ***
    upcxx::barrier();

    // End update timed section
    auto T1 = chrono::steady_clock::now();
    chrono::duration<double> t_walltime = T1-T0;
    double walltime = t_walltime.count();

    /* Verification of results (in parallel).  */
    uint64 temp = 0x1;

    for (i=0; i<nupdate; i++) {
      temp = (temp << 1) ^ (((int64) temp < 0) ? POLY : 0);
      const uint64 offs = temp & tabszm1;
      if ((offs >= (uint64_t)i0) && (offs <= (uint64_t)i1))
          pt[temp & ltabszm1] ^= temp;
    }
	
    int64 nerrors;
    for (i = i0, j=0, nerrors = 0; i <= i1; i++, j++)
        nerrors += (pt[j] != (uint64_t)i);

    uint64 totalErrors = reduce_all(nerrors, upcxx::op_fast_add).wait();
	
    reportResults(walltime_i, walltime, nupdate, ltabsize, totalErrors,thresh,Vlen);

    ad_ui64.destroy();
    delete [] t1;
    delete [] ldirectory;
    delete_array(table);
    return;
}
