// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//
//  update_table -- multi-threaded update_table for 2 or more threads
//  RPC implementation.
//  There are two flavors, one uses promises, the other when_all
//
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <chrono>
#include <upcxx/upcxx.hpp>

#include "block.cpp"
#include "startr.cpp"
#include "guppie.hpp"
#include "guppie-upcxx.hpp"

using namespace std;
using namespace upcxx;

#if RPC_FF
const char *alg_name = "RPC_FF";
#else
const char *alg_name = "RPC";
#endif
int alg_error_free = 1; // RPC updates serialized in upcxx::progress

// Main table
uint64 *table;

#if RPC_FF
int64 ucount[2] = { 0, 0 };

void sync(int64 nbatch, int phase){
    while( reduce_all(ucount[phase], upcxx::op_fast_add).wait() < nbatch)
        progress();
    ucount[phase] = 0;
}
#endif

void update_table(int myrank, int nranks, int Vlen,
                  int   ltabsize,
		  int64 tabsize, int64 nupdate, int freq, double thresh)
{
    int64 i, j;
    uint64  ran;
    uint64  *t1   = new uint64[Vlen];

    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);
//    ostringstream sl;
//    sl << "[" << myrank << "]: " <<  i0 << ", " << i1 << endl;
//    OrderlyOutput(sl);
    table = new uint64[locTabSize];
    assert(table);
#if RPC_FF
    int phase = 0;
#endif

    upcxx::barrier();

    // Begin init timing here
    auto T0_i = chrono::steady_clock::now();

    // Initialize main table
    for(i = i0, j=0; i <= i1; i++, j++)         // converts to local index
	table[j] = i;

    upcxx::barrier();

    // End init timing here
    auto T1_i = chrono::steady_clock::now();
    chrono::duration<double> t_walltime_i = T1_i-T0_i;
    double walltime_i = t_walltime_i.count();

    int64 start, stop, size;
    upcxx::barrier();

    // Begin update timing here
    auto T0 = chrono::steady_clock::now();
    
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;
    ran = startr(start);

    const int64 tabszm1 = tabsize-1;
    const int64 iTabSz = log2(locTabSize);
    const int64 ltabszm1 = locTabSize-1;
    const int   freqm1 = freq-1;
    for (i=0; i<nbatch; i++)
    {
#if RPC_FF
	for (j=0; j<Vlen; j++)
	{
	    ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
            const int64 indxG = ran & tabszm1;
            const uint64 offs = indxG & ltabszm1;    // Determine offset;
            const uint64 targ = indxG >> iTabSz;     // Determine target;
            rpc_ff(targ,
                   [](const uint64 idx, uint64 r, int phase){table[idx] ^= r; ucount[phase]++;},
                   offs, ran, phase);
            if (!(j & freqm1))              // freq must be a power of 2
                progress();
	}
        // Synchronize on completion
        sync(Vlen*nranks,phase);
        phase = 1-phase;
#elif FUTURE // round-trip RPC variant synchronized using futures, not recommended 
        future<> fut = make_future();
	for (j=0; j<Vlen; j++)
	{
	    ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
            const int64 indxG = ran & tabszm1;
            const uint64 offs = indxG & ltabszm1;   // Determine offset;
            const uint64 targ = indxG >> iTabSz;    // Determine target;
            fut = when_all(fut, rpc(targ,
                                    [](const int64 idx, uint64 r){table[idx] ^= r;},
                                    offs, ran));
            if (!(Vlen & freqm1))                   // freq must be a power of 2
                progress();
	}
        fut.wait();
#else // round-trip RPC variant synchronized using promises
        promise<> prom;
	for (j=0; j<Vlen; j++)
	{
	    ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
            const int64 indxG = ran & tabszm1;       // Truncate to global table size
            const uint64 offs = indxG & ltabszm1;  // Determine offset;
            const uint64 targ =  indxG >> iTabSz; // Determine target;

            rpc(targ,upcxx::operation_cx::as_promise(prom),
                [](const int64 idx, uint64 r){table[idx] ^= r;},
                offs, ran);

            if (!(j & freqm1))                   // freq must be a power of 2
                progress();
	}

        future<> fut = prom.finalize();
        fut.wait();
#endif
    }

    // Need this barrier in this fine-grained RPC guppie to ensure
    // global quiescence before beginning verification.
    // Otherwise you could get verification failures with sufficient load
    // imbalance, and/or contamination of the timed region with concurrent
    // verification code.
    upcxx::barrier();
    // End update timed section
    auto T1 = chrono::steady_clock::now();
    chrono::duration<double> t_walltime = T1-T0;
    double walltime = t_walltime.count();

    /* Verification of results (in parallel).  */
    uint64 temp = 0x1;
    const int64 lTabszm1 = locTabSize-1;
    for (i=0; i<nupdate; i++) {
      temp = (temp << 1) ^ (((int64) temp < 0) ? POLY : 0);
      const uint64 offs = temp & tabszm1;
      if ((offs >= (uint64_t)i0) && (offs <= (uint64_t)i1)){
        uint64 *p = & table[temp & lTabszm1];
        *p ^= temp;
      }
    }
	
	
    int64 nerrors;
    for (i = i0, j=0, nerrors = 0; i <= i1; i++, j++)
        nerrors += (table[j] != (uint64_t)i);

    uint64 totalErrors = reduce_all(nerrors, upcxx::op_fast_add).wait();
	
    reportResults(walltime_i, walltime, nupdate, ltabsize, totalErrors,thresh,Vlen);
    delete [] t1;
    delete [] table;
}
