# HPC Challenge RandomAccess #

This directory contains various implementations of the 
HPC Challenge RandomAccess benchmark (aka "gups").

Except where otherwise noted, all files in this directory are 
authored by Scott Baden, and subject to the copyright notice 
and licensing terms in the top-level [LICENSE.txt](../../LICENSE.txt).

The UPC++ implementations require UPC++ 1.0 v2018.9.0 or later,
which is available [here](https://upcxx.lbl.gov).
The AMO version requires a bug fix introduced in 2018.9.3.

For benchmark specification and rules, see:
http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/

