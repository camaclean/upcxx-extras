/*
 * Portions of the code in this file are adapted from STRUMPACK, see strumpack/license.txt
 *
 */
#ifndef DISTRIBUTED_MATRIX_HPP
#define DISTRIBUTED_MATRIX_HPP

#include <cstddef>
#include <random>
#include <functional>
#include <limits>
#include <iostream>
#include <iomanip>

#include "Random_wrapper.hpp"
#include "blas_lapack_wrapper.hpp"

#include "Utils.hpp"

#ifdef _PROXY_MPI_BACKEND_
#include "MPI_wrapper.hpp"
#include "scalapack.hpp"
#else
#include "blacs_upcxx.hpp"
#include "scalapack_upcxx.hpp"
#endif

namespace strumpack {

  inline int
  indxl2g(int INDXLOC, int NB, int IPROC, int ISRCPROC, int NPROCS)
  { return NPROCS*NB*((INDXLOC-1)/NB) + (INDXLOC-1) % NB +
      ((NPROCS+IPROC-ISRCPROC) % NPROCS)*NB + 1; }
  inline int
  indxg2l(int INDXGLOB, int NB, int IPROC, int ISRCPROC, int NPROCS)
  { return NB*((INDXGLOB-1)/(NB*NPROCS)) + (INDXGLOB-1) % NB + 1; }
  inline int
  indxg2p(int INDXGLOB, int NB, int IPROC, int ISRCPROC, int NPROCS)
  { return ( ISRCPROC + (INDXGLOB - 1) / NB ) % NPROCS; }

  template<typename scalar_t> class DistributedMatrix {
    using real_t = typename RealType<scalar_t>::value_type;

  protected:
    scalar_t* _data = nullptr;
    int _desc[9];
    int _lrows;
    int _lcols;
    int _prows;
    int _pcols;
    int _prow;
    int _pcol;

  public:
    DistributedMatrix();
#ifdef _PROXY_MPI_BACKEND_
    DistributedMatrix(int ctxt, int M, int N,
                      const DistributedMatrix<scalar_t>& m, int ctxt_all);
    DistributedMatrix(int ctxt, int M, int N);
    DistributedMatrix(int ctxt, int M, int N, int MB, int NB);
    DistributedMatrix(int desc[9]);
#else
    DistributedMatrix(blacs_upcxx::team_desc * tdesc, int M, int N);
    DistributedMatrix(blacs_upcxx::team_desc * tdesc, int M, int N, int MB, int NB);
#endif
    DistributedMatrix(const DistributedMatrix<scalar_t>& m);
    DistributedMatrix(DistributedMatrix<scalar_t>&& m);
    virtual ~DistributedMatrix();

    DistributedMatrix<scalar_t>&
    operator=(const DistributedMatrix<scalar_t>& m);
    DistributedMatrix<scalar_t>&
    operator=(DistributedMatrix<scalar_t>&& m);

    virtual int rows() const { return _desc[2]; }
    virtual int cols() const { return _desc[3]; }
    inline int lrows() const { return _lrows; }
    inline int lcols() const { return _lcols; }
    inline int ld() const { return _lrows; }
    inline int MB() const { return _desc[4]; }
    inline int NB() const { return _desc[5]; }
    inline int rowblocks() const { return std::ceil(float(lrows()) / MB()); }
    inline int colblocks() const { return std::ceil(float(lcols()) / NB()); }

    virtual int I() const { return 1; }
    virtual int J() const { return 1; }
    virtual void lranges(int& rlo, int& rhi, int& clo, int& chi) const;

    inline const scalar_t* data() const { return _data; }
    inline scalar_t* data() { return _data; }
    /*inline*/ const scalar_t& operator()(int r, int c) const
    { return _data[r+ld()*c]; }
    /*inline*/ scalar_t& operator()(int r, int c) { return _data[r+ld()*c]; }

    inline int prow() const { return _prow; }
    inline int pcol() const { return _pcol; }
    inline int prows() const { return _prows; }
    inline int pcols() const { return _pcols; }
    inline int procs() const { return (prows() == -1) ? 0 : prows()*pcols(); }
    inline bool is_master() const { return prow() == 0 && pcol() == 0; }
    inline int rowl2g(int row) const { assert(_prow != -1);
      return indxl2g(row+1, MB(), prow(), 0, prows()) - I(); }
    inline int coll2g(int col) const { assert(_pcol != -1);
      return indxl2g(col+1, NB(), pcol(), 0, pcols()) - J(); }
    inline int rowg2l(int row) const { assert(_prow != -1);
      return indxg2l(row+I(), MB(), prow(), 0, prows()) - 1; }
    inline int colg2l(int col) const { assert(_pcol != -1);
      return indxg2l(col+J(), NB(), pcol(), 0, pcols()) - 1; }
    inline int rowg2p(int row) const { assert(_prow != -1);
      return indxg2p(row+I(), MB(), prow(), 0, prows()); }
    inline int colg2p(int col) const { assert(_pcol != -1);
      return indxg2p(col+J(), NB(), pcol(), 0, pcols()); }
    inline int rank(int r, int c) const {
      return rowg2p(r) + colg2p(c) * prows(); }
    inline bool is_local(int r, int c) const { assert(_prow != -1);
      return rowg2p(r) == prow() && colg2p(c) == pcol(); }

    inline bool fixed() const { return MB()==default_MB && NB()==default_NB; }
    inline int rowl2g_fixed(int row) const {
      assert(_prow != -1); assert(fixed());
      return indxl2g(row+1, default_MB, prow(), 0, prows()) - I(); }
    inline int coll2g_fixed(int col) const {
      assert(_pcol != -1); assert(fixed());
      return indxl2g(col+1, default_NB, pcol(), 0, pcols()) - J(); }
    inline int rowg2l_fixed(int row) const {
      assert(_prow != -1); assert(fixed());
      return indxg2l(row+I(), default_MB, prow(), 0, prows()) - 1; }
    inline int colg2l_fixed(int col) const {
      assert(_pcol != -1); assert(fixed());
      return indxg2l(col+J(), default_NB, pcol(), 0, pcols()) - 1; }
    inline int rowg2p_fixed(int row) const {
      assert(_prow != -1); assert(fixed());
      return indxg2p(row+I(), default_MB, prow(), 0, prows()); }
    inline int colg2p_fixed(int col) const {
      assert(_pcol != -1); assert(fixed());
      return indxg2p(col+J(), default_NB, pcol(), 0, pcols()); }
    inline int rank_fixed(int r, int c) const {
      assert(fixed()); return rowg2p_fixed(r) + colg2p_fixed(c) * prows(); }
    inline bool is_local_fixed(int r, int c) const {
      assert(_prow != -1); assert(fixed());
      return rowg2p_fixed(r) == prow() && colg2p_fixed(c) == pcol(); }

    inline const int* desc() const { return _desc; }
    inline int* desc() { return _desc; }
    inline bool active() const { return _prow != -1; }
#ifndef _PROXY_MPI_BACKEND_
    blacs_upcxx::team_desc * _tdesc;
    inline const blacs_upcxx::team_desc * tdesc() const { return _tdesc; }
#else
    inline int ctxt() const { return _desc[1]; }
#endif

    // TODO fixed versions??
    /*inline*/ const scalar_t& global(int r, int c) const
    { assert(is_local(r, c)); return operator()(rowg2l(r),colg2l(c)); }
    inline scalar_t& global(int r, int c)
    { assert(is_local(r, c)); return operator()(rowg2l(r),colg2l(c)); }
    /*inline*/ scalar_t& global_fixed(int r, int c) {
      assert(is_local(r, c)); assert(fixed());
      return operator()(rowg2l_fixed(r),colg2l_fixed(c)); }
    inline void global(int r, int c, scalar_t v) {
      if (active() && is_local(r, c)) operator()(rowg2l(r),colg2l(c)) = v;  }
    inline scalar_t all_global(int r, int c) const;

    void print() const { print("A"); }
    void print(std::string name, int precision=15) const;
    void print_to_file(std::string name, std::string filename,
                       int width=8) const;
    void random();
    void random(random::RandomGeneratorBase<typename RealType<scalar_t>::
                value_type>& rgen);
    void zero();
    void fill(scalar_t a);
    void eye();
    void clear();

#ifdef _PROXY_MPI_BACKEND_
    virtual void resize(std::size_t m, std::size_t n);
    DistributedMatrix<scalar_t> transpose() const;
#endif

    virtual std::size_t memory() const
    { return sizeof(scalar_t)*lrows()*lcols(); }
    virtual std::size_t total_memory() const
    { return sizeof(scalar_t)*rows()*cols(); }
    virtual std::size_t nonzeros() const { return lrows()*lcols(); }
    virtual std::size_t total_nonzeros() const { return rows()*cols(); }


    std::vector<int> LU();
#ifdef STRUMPACK_PBLAS_BLOCKSIZE
    static const int default_MB = STRUMPACK_PBLAS_BLOCKSIZE;
    static const int default_NB = STRUMPACK_PBLAS_BLOCKSIZE;
#else
    static const int default_MB = 32;
    static const int default_NB = 32;
#endif
  };

#ifndef _PROXY_MPI_BACKEND_
  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix()
    : DistributedMatrix(nullptr, 0, 0, default_MB, default_NB) {
    // make sure active() returns false if not constructed properly
    _prow = _pcol = _prows = _pcols = -1;
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix
  (const DistributedMatrix<scalar_t>& m)
    : _lrows(m._lrows), _lcols(m._lcols), _prows(m._prows), _pcols(m._pcols),
      _prow(m._prow), _pcol(m._pcol) {
    std::copy(m._desc, m._desc+9, _desc);
    delete[] _data;
    _data = new scalar_t[_lrows*_lcols];
    std::copy(m._data, m._data+_lrows*_lcols, _data);
    _tdesc = new blacs_upcxx::team_desc(m._tdesc->team,m._tdesc->nprow,m._tdesc->npcol);
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix
  (DistributedMatrix<scalar_t>&& m)
    : _lrows(m._lrows), _lcols(m._lcols), _prows(m._prows), _pcols(m._pcols),
      _prow(m._prow), _pcol(m._pcol) {
    std::copy(m._desc, m._desc+9, _desc);
    delete[] _data;
    _data = m._data;
    m._data = nullptr;
    _tdesc = m._tdesc;
    m._tdesc = nullptr;
  }

  template<typename scalar_t>
  DistributedMatrix<scalar_t>::DistributedMatrix(blacs_upcxx::team_desc * tdesc, int M, int N)
    : DistributedMatrix(tdesc, M, N, default_MB, default_NB) {
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix
  (blacs_upcxx::team_desc * tdesc, int M, int N, int MB, int NB)
  {
    assert(M >= 0 && N >= 0 && MB >= 0 && NB >= 0);
    MB = std::max(1, MB);
    NB = std::max(1, NB);

    if ( tdesc != nullptr ) {
    _tdesc = new blacs_upcxx::team_desc(tdesc->team,tdesc->nprow,tdesc->npcol);
    _prows = tdesc->nprow; _pcols = tdesc->npcol;
    _prow = tdesc->myrow; _pcol = tdesc->mycol;
    }
    else {
    _tdesc = nullptr;
    _prows = 0; _pcols = 0;
    _prow = -1; _pcol = -1;
    }
    int ctxt = 0;
    if (_prow == -1 || _pcol == -1) {
      _lrows = _lcols = 0;
      _data = nullptr;
      scalapack::descset(_desc, M, N, MB, NB, 0, 0, ctxt, std::max(_lrows,1));
    } else {
      //TODO initialize this
      _lrows = scalapack::numroc(M, MB, _prow, 0, _prows);
      _lcols = scalapack::numroc(N, NB, _pcol, 0, _pcols);
      _data = new scalar_t[_lrows*_lcols];
      //TODO initialize _desc
      if (scalapack::descinit(_desc, M, N, MB, NB, 0, 0,
                              *_tdesc, std::max(_lrows,1))) {
        std::cerr << "ERROR: Could not create DistributedMatrix descriptor!"
                  << std::endl;
        abort();
      }
    }
  }

#else
  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix()
    : DistributedMatrix(-1, 0, 0, default_MB, default_NB) {
    // make sure active() returns false if not constructed properly
    _prow = _pcol = _prows = _pcols = -1;
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix
  (int ctxt, int M, int N, const DistributedMatrix<scalar_t>& m, int ctxt_all)
    : DistributedMatrix(ctxt, M, N, default_MB, default_NB) {
    //strumpack::copy(M, N, m, 0, 0, *this, 0, 0, ctxt_all);
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix
  (const DistributedMatrix<scalar_t>& m)
    : _lrows(m._lrows), _lcols(m._lcols), _prows(m._prows), _pcols(m._pcols),
      _prow(m._prow), _pcol(m._pcol) {
    std::copy(m._desc, m._desc+9, _desc);
    delete[] _data;
    _data = new scalar_t[_lrows*_lcols];
    std::copy(m._data, m._data+_lrows*_lcols, _data);
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix
  (DistributedMatrix<scalar_t>&& m)
    : _lrows(m._lrows), _lcols(m._lcols), _prows(m._prows), _pcols(m._pcols),
      _prow(m._prow), _pcol(m._pcol) {
    std::copy(m._desc, m._desc+9, _desc);
    delete[] _data;
    _data = m._data;
    m._data = nullptr;
  }

  template<typename scalar_t>
  DistributedMatrix<scalar_t>::DistributedMatrix(int ctxt, int M, int N)
    : DistributedMatrix(ctxt, M, N, default_MB, default_NB) {
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>::DistributedMatrix
  (int ctxt, int M, int N, int MB, int NB) {
    assert(M >= 0 && N >= 0 && MB >= 0 && NB >= 0);
    MB = std::max(1, MB);
    NB = std::max(1, NB);

    Cblacs_gridinfo(ctxt, &_prows, &_pcols, &_prow, &_pcol);
    if (_prow == -1 || _pcol == -1) {
      _lrows = _lcols = 0;
      _data = nullptr;
      scalapack::descset(_desc, M, N, MB, NB, 0, 0, ctxt, std::max(_lrows,1));
    } else {
      //TODO initialize this
      _lrows = scalapack::numroc(M, MB, _prow, 0, _prows);
      _lcols = scalapack::numroc(N, NB, _pcol, 0, _pcols);
      _data = new scalar_t[_lrows*_lcols];
      //TODO initialize _desc
      if (scalapack::descinit(_desc, M, N, MB, NB, 0, 0,
                              ctxt, std::max(_lrows,1))) {
        std::cerr << "ERROR: Could not create DistributedMatrix descriptor!"
                  << std::endl;
        abort();
      }
    }
  }

  template<typename scalar_t>
  DistributedMatrix<scalar_t>::DistributedMatrix(int desc[9]) {
    std::copy(desc, desc+9, _desc);
    Cblacs_gridinfo(_desc[1], &_prows, &_pcols, &_prow, &_pcol);
    if (_prow == -1 || _pcol == -1) {
      _lrows = _lcols = 0;
      _data = nullptr;
    } else {
      //TODO initialize this
      _lrows = scalapack::numroc(_desc[2], _desc[4], _prow, _desc[6], _prows);
      _lcols = scalapack::numroc(_desc[3], _desc[5], _pcol, _desc[7], _pcols);
//      assert(_lrows==_desc[8]);
      if (_lrows && _lcols) _data = new scalar_t[_lrows*_lcols];
      else _data = nullptr;
    }
  }
#endif

  template<typename scalar_t>
  DistributedMatrix<scalar_t>::~DistributedMatrix() {
    clear();
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>&
  DistributedMatrix<scalar_t>::operator=
  (const DistributedMatrix<scalar_t>& m) {
    _prows = m._prows;  _pcols = m._pcols;
    _prow = m._prow;    _pcol = m._pcol;
    _lrows = m._lrows;  _lcols = m._lcols;
    std::copy(m._desc, m._desc+9, _desc);
    delete[] _data;
    _data = new scalar_t[_lrows*_lcols];
    std::copy(m._data, m._data+_lrows*_lcols, _data);
#ifndef _PROXY_MPI_BACKEND_
    _tdesc = new blacs_upcxx::team_desc(m._tdesc->team,m._tdesc->nprow,m._tdesc->npcol);
#endif
    return *this;
  }

  template<typename scalar_t> DistributedMatrix<scalar_t>&
  DistributedMatrix<scalar_t>::operator=(DistributedMatrix<scalar_t>&& m) {
    _prows = m._prows;  _pcols = m._pcols;
    _prow = m._prow;    _pcol = m._pcol;
    _lrows = m._lrows;  _lcols = m._lcols;
    std::copy(m._desc, m._desc+9, _desc);
    delete[] _data;
    _data = m._data;
    m._data = nullptr;
#ifndef _PROXY_MPI_BACKEND_
    _tdesc = m._tdesc;
    m._tdesc = nullptr;
#endif
    return *this;
  }

  template<typename scalar_t> void DistributedMatrix<scalar_t>::lranges
  (int& rlo, int& rhi, int& clo, int& chi) const {
    rlo = clo = 0;
    rhi = lrows();
    chi = lcols();
  }

  template<typename scalar_t> void DistributedMatrix<scalar_t>::clear() {
    delete[] _data;
    _data = nullptr;
    _prow = _pcol = _prows = _pcols = -1;
    _lrows = _lcols = 0;
#ifndef _PROXY_MPI_BACKEND_
    int ictxt = 0;
    delete _tdesc;
    _tdesc = nullptr;
#else
    int ictxt = ctxt();
#endif
    scalapack::descset(_desc, 0, 0, MB(), NB(), 0, 0, ictxt, 1);
  }

#ifdef _PROXY_MPI_BACKEND_
  template<typename scalar_t> void DistributedMatrix<scalar_t>::resize
  (std::size_t m, std::size_t n) {
    DistributedMatrix<scalar_t> tmp(ctxt(),m, n, MB(), NB());
    for (int c=0; c<std::min(lcols(), tmp.lcols()); c++)
      for (int r=0; r<std::min(lrows(), tmp.lrows()); r++)
        tmp(r, c) = operator()(r, c);
    *this = std::move(tmp);
  }
#endif

  template<typename scalar_t> void DistributedMatrix<scalar_t>::zero() {
    if (!active()) return;
    int rlo, rhi, clo, chi;
    lranges(rlo, rhi, clo, chi);
    for (int c=clo; c<chi; c++)
      for (int r=rlo; r<rhi; r++)
        operator()(r,c) = scalar_t(0.);
  }

  template<typename scalar_t> void
  DistributedMatrix<scalar_t>::fill(scalar_t a) {
    if (!active()) return;
    int rlo, rhi, clo, chi;
    lranges(rlo, rhi, clo, chi);
    for (int c=clo; c<chi; c++)
      for (int r=rlo; r<rhi; r++)
        operator()(r,c) = a;
  }

  template<typename scalar_t> void DistributedMatrix<scalar_t>::random() {
    if (!active()) return;
    auto rgen = random::make_default_random_generator<real_t>();
    rgen->seed(_prow, _pcol);
    int rlo, rhi, clo, chi;
    lranges(rlo, rhi, clo, chi);
    for (int c=clo; c<chi; ++c)
      for (int r=rlo; r<rhi; ++r)
        operator()(r,c) = rgen->get();
  }

  template<typename scalar_t> void DistributedMatrix<scalar_t>::random
  (random::RandomGeneratorBase<typename RealType<scalar_t>::
   value_type>& rgen) {
    if (!active()) return;
    int rlo, rhi, clo, chi;
    lranges(rlo, rhi, clo, chi);
    for (int c=clo; c<chi; ++c)
      for (int r=rlo; r<rhi; ++r)
        operator()(r,c) = rgen.get();
  }

  template<typename scalar_t> void DistributedMatrix<scalar_t>::eye() {
    if (!active()) return;
    int rlo, rhi, clo, chi;
    lranges(rlo, rhi, clo, chi);
    // TODO set to zero, then a single loop for the diagonal
    for (int c=clo; c<chi; ++c)
      for (int r=rlo; r<rhi; ++r)
        operator()(r,c) = (rowl2g(r)-I()+1 == coll2g(c)-J()+1) ?
          scalar_t(1.) : scalar_t(0.);
  }

  /** correct value only on the procs in the ctxt */
  template<typename scalar_t> scalar_t
  DistributedMatrix<scalar_t>::all_global(int r, int c) const {
    if (!active()) return scalar_t(0.);
    scalar_t v;
    if (is_local(r, c)) {
      v = operator()(rowg2l(r), colg2l(c));
    }
    return v;
  }



#ifdef _PROXY_MPI_BACKEND_
  template<typename scalar_t> DistributedMatrix<scalar_t>
  DistributedMatrix<scalar_t>::transpose() const {
    DistributedMatrix<scalar_t> tmp(ctxt(),cols(), rows());
    if (!active()) return tmp;
    return tmp;
  }
#endif

  template<typename scalar_t> std::vector<int>
  DistributedMatrix<scalar_t>::LU() {
    if (!active()) return std::vector<int>();
    std::vector<int> ipiv(lrows()+MB());
    return ipiv;
  }

  template<typename scalar_t> void gemm
  (Trans ta, Trans tb, scalar_t alpha, const DistributedMatrix<scalar_t>& A,
   const DistributedMatrix<scalar_t>& B,
   scalar_t beta, DistributedMatrix<scalar_t>& C) {
    if (!A.active()) return;
    assert((ta==Trans::N && A.rows()==C.rows()) ||
           (ta!=Trans::N && A.cols()==C.rows()));
    assert((tb==Trans::N && B.cols()==C.cols()) ||
           (tb!=Trans::N && B.rows()==C.cols()));
    assert((ta==Trans::N && tb==Trans::N && A.cols()==B.rows()) ||
           (ta!=Trans::N && tb==Trans::N && A.rows()==B.rows()) ||
           (ta==Trans::N && tb!=Trans::N && A.cols()==B.cols()) ||
           (ta!=Trans::N && tb!=Trans::N && A.rows()==B.cols()));
    assert(A.I()>=1 && A.J()>=1 && B.I()>=1 &&
           B.J()>=1 && C.I()>=1 && C.J()>=1);
  }

  template<typename scalar_t> void trsm
  (Side s, UpLo u, Trans ta, Diag d, scalar_t alpha,
   const DistributedMatrix<scalar_t>& A, DistributedMatrix<scalar_t>& B) {
    if (!A.active()) return;
    assert(A.rows()==A.cols());
    assert(s!=Side::L || ta!=Trans::N || A.cols()==B.rows());
    assert(s!=Side::L || ta==Trans::N || A.rows()==B.rows());
    assert(s!=Side::R || ta!=Trans::N || A.rows()==B.cols());
    assert(s!=Side::R || ta==Trans::N || A.cols()==B.cols());
  }

  template<typename scalar_t> void trsv
  (UpLo ul, Trans ta, Diag d, const DistributedMatrix<scalar_t>& A,
   DistributedMatrix<scalar_t>& B) {
    if (!A.active()) return;
    assert(B.cols() == 1 && A.rows() == A.cols() && A.cols() == A.rows());
  }

  template<typename scalar_t> void gemv
  (Trans ta, scalar_t alpha, const DistributedMatrix<scalar_t>& A,
   const DistributedMatrix<scalar_t>& X, scalar_t beta,
   DistributedMatrix<scalar_t>& Y) {
    if (!A.active()) return;
    assert(X.cols() == 1 && Y.cols() == 1);
    assert(ta != Trans::N || (A.rows() == Y.rows() && A.cols() == X.rows()));
    assert(ta == Trans::N || (A.cols() == Y.rows() && A.rows() == X.rows()));
  }

#ifdef _PROXY_MPI_BACKEND_
  template<typename scalar_t> DistributedMatrix<scalar_t> vconcat
  (int cols, int arows, int brows, const DistributedMatrix<scalar_t>& a,
   const DistributedMatrix<scalar_t>& b, int ctxt_new, int ctxt_all) {
    DistributedMatrix<scalar_t> tmp(ctxt_new, arows+brows, cols);
    copy(arows, cols, a, 0, 0, tmp, 0, 0, ctxt_all);
    copy(brows, cols, b, 0, 0, tmp, arows, 0, ctxt_all);
    return tmp;
  }
#endif

  template<typename scalar_t> void
  DistributedMatrix<scalar_t>::print(std::string name, int precision) const {
    if (!active()) return;
    std::stringstream sstr;

    int width = 8;
    sstr << name << " = [  % " << rows() << "x" << cols()
              << ", ld=" << ld() <<  std::endl;
      for (std::size_t i=0; i<rows(); i++) {
        for (std::size_t j=0; j<cols(); j++)
          sstr << std::setw(width) << operator()(i,j) << "  ";
        sstr << std::endl;
      }
    sstr << "];" << std::endl << std::endl;

    logfile<<sstr.str();
  }

  template<typename scalar_t> void
  DistributedMatrix<scalar_t>::print_to_file
  (std::string name, std::string filename, int width) const {
    if (!active()) return;
  }

} // end namespace strumpack

#endif // DISTRIBUTED_MATRIX_HPP
