#Extend-add example

## Overview
This code is a proxy application for the extend-add operation on sparse matrices.
Extend-add is a fundamental operation of multifrontal sparse direct solver.

Portions of the code are adapted from the [UPCProxy](https://github.com/pghysels/STRUMPACK/tree/UPCProxy) 
branch of STRUMPACK, which license is available in strumpack/license.txt.

This implementation depends on:

  - A working UPC++ v1.0 implementation (2019.3.0 or later), with the upcxx
    compiler in the user's PATH or the UPCXX_INSTALL environment variable set.
  - A Scalapack installation, with the environment variables SCALAPACK_INSTALL set appropriately.

The provided Makefile is just one example of building this application on a
Linux cluster, given some external include directories and libraries containing
the definition and implementation of a third-party Scalapack library. 
This Makefile will likely need to be tweaked depending on your system's configuration.

## Running the code

Input files generated with the STRUMPACK solver for two
sparse matrices from the SuiteSparse matrix collection, *audikw_1* and *dielFilterV3real*,
are available at:
https://bitbucket.org/berkeleylab/upcxx-extras/downloads/extend-add-audikw_1.tar.gz 
and https://bitbucket.org/berkeleylab/upcxx-extras/downloads/extend-add-dielFilterV3real.tar.gz

After extracting the input files in the compressed archives, the code can be run by simply executing:
```
upcxx-run -shared-heap YGB -n X bin/extend-add_upcxx /PATH/TO/%MATRIXNAME%_X.dmp
mpirun -n X bin/extend-add_mpi_collective /PATH/TO/%MATRIXNAME%_X.dmp
mpirun -n X bin/extend-add_mpi_p2p /PATH/TO/%MATRIXNAME%_X.dmp
```

Note that UPC++ should be allowed to use up to `Y` GB of memory for the shared memory segment. 
The appropriate value of Y depends on the input problem and how many non-zeros are present in
the largest contribution block being sent when performing the extend-add operation. 
On UNIX systems, we suggest the use of the **get_max_memusage.sh** script located in the *utils* directory
to determine the proper -shared-heap option. This script is used in the following way:
```
bash utils/get_max_memusage.sh /PATH/TO/%MATRIXNAME%_X.dmp
```

