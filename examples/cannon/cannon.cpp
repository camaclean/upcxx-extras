#include <upcxx/upcxx.hpp>

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#include <cblas.h>
#endif
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>

#if UPCXX_VERSION < 20190300
#error This test requires UPC++ 2019.3.0 or newer (issue #141, upcxx command, cuda support)
#endif

#ifdef USE_CUDA
#include "initMatrix.hpp"

#define CHECK_CUDA(call) { \
    cudaError_t err = (call); \
    if (err != cudaSuccess) { \
        fprintf(stderr, "CUDA ERROR @ %s:%d - %s\n", __FILE__, __LINE__, \
                cudaGetErrorString(err)); \
        abort(); \
    } \
}

#define CHECK_CUBLAS(call) { \
    cublasStatus_t stat = (call); \
    if (stat != CUBLAS_STATUS_SUCCESS) { \
        fprintf(stderr, "CUBLAS ERROR @ %s:%d\n", __FILE__, __LINE__); \
        abort(); \
    } \
}

#include "cublas_v2.h"
#include <cuda.h>
#include <cuda_runtime.h>
#endif // USE_CUDA

#undef I // sometimes captured by headers above including complex.h

using namespace std;

/*
 * An example implementation of Cannon's algorithm in UPC++. See the provided
 * README.txt for more details.
 *
 * This code was primarily authored by Brian Van Straalen (bvstraalen@lbl.gov)
 * with modifications by Max Grossman (jonathangrossman@lbl.gov).
 */

#ifdef USE_CUDA
const upcxx::memory_kind memkind = upcxx::memory_kind::cuda_device;
#else
const upcxx::memory_kind memkind = upcxx::memory_kind::host;
#endif

typedef   upcxx::dist_object<upcxx::global_ptr<double, memkind>> dmatrix;

int main (int argc, char* argv[]) {
    upcxx::init(); 

#ifdef USE_CUDA
    cublasHandle_t handle;
    CHECK_CUBLAS(cublasCreate(&handle));
#endif

    const int nranks = upcxx::rank_n();
    const int me = upcxx::rank_me();

    if (argc != 2) {
        if (!upcxx::rank_me())
          cout<<"usage: "<<argv[0]<<" N \n where N is the dimension of the " <<
            "matrices to multiply\n";
        return 1;
    }
    int N = atoi(argv[1]);

    int rootP = std::sqrt(nranks);
    if (rootP * rootP != nranks) {
        if (!upcxx::rank_me())
          cout<<"sorry, Cannon's algorithm requires a perfect square number " <<
            "of ranks\n";
        return 2;
    }

    upcxx::barrier(); // Barrier for timing, not correctness
    std::chrono::steady_clock::time_point start_setup =
        std::chrono::steady_clock::now();

    int blocksize = (N+rootP-1)/rootP; //round-upwards divide
    int M = blocksize*blocksize;
    int actualN = blocksize * rootP;
    if (me == 0) {
        if (actualN != N) {
            cerr << "Warning: resizing from " << N << "x" << N << " to " <<
                actualN << "x" << actualN << " to fit # of ranks." << endl;
        }
        cout << "Running Cannon's algorithm for " << actualN << "x" <<
            actualN << " matrix multiplication." << endl;
    }

#ifdef USE_CUDA
   auto gpu_device = upcxx::cuda_device(0);

   /*
    * Allocator heap size must be padded out because allocator may waste up to 1
    * page of memory per allocation.
    */
   size_t buff_size = M * 6 * sizeof(double);
   const size_t mb = 1024 * 1024;
   buff_size = buff_size + (mb - (buff_size % mb));

   auto gpu_alloc = upcxx::device_allocator<upcxx::cuda_device>(gpu_device,
           buff_size);
   upcxx::global_ptr<double, upcxx::memory_kind::cuda_device> buff =
       gpu_alloc.allocate<double>(M * 6);
   if (!buff) {
       fprintf(stderr, "Failed allocating %llu bytes on device 0\n",
               (unsigned long long)(M * 6 * sizeof(double)));
       abort();
   }
#else
   upcxx::global_ptr<double, upcxx::memory_kind::host> buff =
       upcxx::new_array<double>(M * 6);
   assert(buff);
#endif

   dmatrix darray(buff);

   upcxx::global_ptr<double, memkind> A = buff;
   upcxx::global_ptr<double, memkind> B = A + M;
   upcxx::global_ptr<double, memkind> A0 = B + M;
   upcxx::global_ptr<double, memkind> B0 = A0 + M;
   upcxx::global_ptr<double, memkind> A1 = B0 + M;
   upcxx::global_ptr<double, memkind> B1 = A1 + M;

    // Local output block
    double* C  = new double[M];

    int I = me / rootP;
    int J = me % rootP;

    upcxx::future<> fut = upcxx::make_future();

    std::vector<upcxx::global_ptr<double, memkind>> a(rootP), b(rootP);
    for (int i = 0; i < rootP; i++) {
        int Aj = (I + i) % rootP;
        int Bi = (I + i) % rootP;
        upcxx::intrank_t Afetch = I * rootP + Aj;
        upcxx::intrank_t Bfetch = Bi * rootP + J;
        fut = upcxx::when_all(fut,
                darray.fetch(Afetch).then([&a,i](upcxx::global_ptr<double, memkind> gp) {
                    a[i]=gp;
                }),
                darray.fetch(Bfetch).then([&b,i](upcxx::global_ptr<double, memkind> gp) {
                    b[i]=gp;
                })
            );
    }

#ifdef USE_CUDA
    initMatrixOnDevice(gpu_alloc.local(A), gpu_alloc.local(B), M, me + 1,
            2 * (me + 1));
    CHECK_CUDA(cudaDeviceSynchronize());
    CHECK_CUDA(cudaGetLastError());
#else
    double *A_local = A.local();
    double *B_local = B.local();
    for (int i = 0; i < M; i++) {
        A_local[i] = me+1;
        B_local[i] = 2*(me+1);
    }
#endif

    fut.wait();

    upcxx::barrier();

    double alpha = 1.0;
    double beta = 1.0;

    upcxx::global_ptr<double, memkind> computeA = A1;
    upcxx::global_ptr<double, memkind> computeB = B1;
    upcxx::global_ptr<double, memkind> rgetA = A0;
    upcxx::global_ptr<double, memkind> rgetB = B0;
    upcxx::promise<> prom[2];
    upcxx::future<> f[2] = {upcxx::make_future(), upcxx::make_future()};

    upcxx::barrier(); // Barrier for timing, not correctness
    std::chrono::steady_clock::time_point start_compute =
        std::chrono::steady_clock::now();

    // peel off first iteration rgets.  overlap with zeroing C
    upcxx::copy(a[0], A1, M, upcxx::operation_cx::as_promise(prom[0]));
    upcxx::copy(b[0] + M, B1, M, upcxx::operation_cx::as_promise(prom[0]));
    f[0] = prom[0].finalize();

#ifdef USE_CUDA
    double *d_C;
    CHECK_CUDA(cudaMalloc(&d_C, M * sizeof(double)));
    CHECK_CUDA(cudaMemset(d_C, 0x00, M * sizeof(double)));
#else
    memset(C, 0, M*sizeof(double));
#endif

    for (int step = 1; step<rootP; step++) {
        int phase = step % 2;
        int lastphase = !phase;

        upcxx::copy(a[step],   rgetA, M,
                upcxx::operation_cx::as_promise(prom[phase]));
        upcxx::copy(b[step]+M, rgetB, M,
                upcxx::operation_cx::as_promise(prom[phase]));
        f[phase] = prom[phase].finalize();

        f[lastphase].wait();
        prom[lastphase] = upcxx::promise<>(); // set back to constructed

#ifdef USE_CUDA
        CHECK_CUBLAS(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
                blocksize, blocksize, blocksize,
                &alpha,
                gpu_alloc.local(computeA), blocksize,
                gpu_alloc.local(computeB), blocksize,
                &beta,
                d_C, blocksize));
        if (step < rootP - 1) { // unncessary on last iteration
            /*
             * Synchronize here to ensure that the asynchronous GEMM kernel
             * above has completed before we issue upcxx::copys on the next
             * iteration that will overwrite the current computeA and computeB
             * (inputs to the GEMM).
             */
            CHECK_CUDA(cudaDeviceSynchronize());
        }
#else
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 
                blocksize, blocksize, blocksize, alpha,
                computeA.local(), blocksize,
                computeB.local(), blocksize,
                beta, C, blocksize);
#endif
        std::swap(computeA, rgetA);
        std::swap(computeB, rgetB);
    }

    f[0].wait(); f[1].wait(); //wait on both, don't know if rootP is even or odd
#ifdef USE_CUDA
    CHECK_CUBLAS(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
            blocksize, blocksize, blocksize,
            &alpha,
            gpu_alloc.local(computeA), blocksize,
            gpu_alloc.local(computeB), blocksize,
            &beta,
            d_C, blocksize));
    // Transfer the final result back to the host
    CHECK_CUDA(cudaMemcpy(C, d_C, blocksize * blocksize * sizeof(*C),
                cudaMemcpyDeviceToHost));
#else
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 
            blocksize, blocksize, blocksize, alpha,
            computeA.local(), blocksize,
            computeB.local(), blocksize,
            beta, C, blocksize);
#endif

    upcxx::barrier(); // Barrier for timing, not correctness
    std::chrono::steady_clock::time_point finish_compute =
        std::chrono::steady_clock::now();

    // check for correctness.  Build scalar version of above blocked matrix, then scale
    // the result with blocksize and sample the right i,j corresponding to this rank subblock
    double* aa=new double[nranks];
    double* bb=new double[nranks];
    double* cc=new double[nranks];
    for (int i=0; i<nranks; i++) {
        aa[i]=i+1;
        bb[i]=2*(i+1);
    }
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 
            rootP, rootP, rootP, blocksize, aa, rootP,
            bb, rootP, 0, cc, rootP);
    int any_err = 0;
    for (int i=0; i<M; i++) {
        if (C[i] != cc[rootP*I+J]) {
            cout<<"ERROR: rank:"<<me<<" expected "<<cc[rootP*I+J]
                <<" got "<<C[i]<< "\n";
            any_err = 1;
            break;
        }
    }

    upcxx::barrier(); // Barrier for timing, not correctness
    std::chrono::steady_clock::time_point finish_verify =
        std::chrono::steady_clock::now();

    if (me == 0) {
        double init_time = std::chrono::duration<double>(start_compute -
                start_setup).count();
        double compute_time = std::chrono::duration<double>(finish_compute -
                start_compute).count();
        double verify_time = std::chrono::duration<double>(finish_verify -
                finish_compute).count();

        cout << "Initialization: " << init_time << " s" << endl;
        cout << "Parallel Compute: " << compute_time << " s" << endl;
        cout << "Verification: " << verify_time << " s" << endl;
    }

    delete[] aa;
    delete[] bb;
    delete[] cc;
    delete[] C;

#ifdef USE_CUDA
    gpu_alloc.deallocate(buff);
    CHECK_CUDA(cudaFree(d_C));

    gpu_device.destroy();
#else
    upcxx::delete_array(buff);
#endif

    upcxx::finalize();

    if (me == 0 && !any_err) {
        cout << "SUCCESS" << endl;
    }

    return 0;
}
