#include <upcxx/upcxx.hpp>

#include <stdio.h>
#include <assert.h>

/*
 * An example remote accumulate function that adds nelements values from a local
 * array (local_src) to a remote array (remote_dst) using an RPC. A UPC++ view
 * is used to automatically ship the contents of local_src to the remote rank.
 *
 * While this function does not use atomic instructions, it is atomic with
 * respect to other RPCs and application code executing on the remote master
 * persona/main thread.
 *
 * This function only works for T that are TriviallySerializable.
 */
template <typename T>
upcxx::future<> add_accumulate(upcxx::global_ptr<T> remote_dst, T const * local_src,
        size_t nelements) {
    return upcxx::rpc(remote_dst.where(), [] (
                    const upcxx::global_ptr<T>& dst, const upcxx::view<T>& src) {
                T *dst_ptr = dst.local();
                T const *src_ptr = src.begin();
                int lim = src.size();
                for (int i = 0; i < lim; i++) {
                    dst_ptr[i] += src_ptr[i];
                }
            }, remote_dst, upcxx::make_view(local_src, local_src + nelements));
}

/*
 * Equivalent to the function above, but can support types that are not
 * TriviallySerializable.
 */
template <typename T>
upcxx::future<> add_accumulate_nt(upcxx::global_ptr<T> remote_dst, T const * local_src,
        size_t nelements) {
    return upcxx::rpc(remote_dst.where(), [] (
                    const upcxx::global_ptr<T>& dst, const upcxx::view<T>& src) {
                T *dst_ptr = dst.local();
                int i = 0;
                for (T elem : src) {
                    dst_ptr[i++] += elem;
                }
            }, remote_dst, upcxx::make_view(local_src, local_src + nelements));
}

struct triple {
    /*
     * unused, just an example of state that would make this struct not
     * trivially serializable.
     */
    void *local_state;

    int x, y, z;

    UPCXX_SERIALIZED_FIELDS(x,y,z)

    triple &operator+=(const triple& b) {
        x += b.x;
        y += b.y;
        z += b.z;
        return *this;
    }
};

/*
 * Example usage of the add_accumulate function above. Each rank adds its rank
 * ID to a shared array on one other rank.
 */
int main(int argc, char **argv) {
    upcxx::init();

    const int nranks = upcxx::rank_n();
    const int me = upcxx::rank_me();
    const int next = (me + 1) % nranks;
    const int prev = (me + nranks - 1) % nranks;

    int N = 1024;

    double *incr = new double[N];
    upcxx::global_ptr<double> recv_buff = upcxx::new_array<double>(N);
    assert(incr && recv_buff);

    triple *triples = new triple[N];
    upcxx::global_ptr<triple> triple_recv_buff = upcxx::new_array<triple>(N);
    assert(triples && triple_recv_buff);

    memset(recv_buff.local(), 0x00, N * sizeof(double));
    memset(triple_recv_buff.local(), 0x00, N * sizeof(triple));
    for (int i = 0; i < N; i++) {
        incr[i] = me;
        triples[i].x = me;
        triples[i].y = me;
        triples[i].z = me;
    }

    upcxx::dist_object<upcxx::global_ptr<double>> dobj(recv_buff);
    upcxx::dist_object<upcxx::global_ptr<triple>> dobj2(triple_recv_buff);

    upcxx::global_ptr<double> target = dobj.fetch(next).wait();
    upcxx::global_ptr<triple> triple_target = dobj2.fetch(next).wait();

    add_accumulate(target, incr, N).wait();
    add_accumulate_nt(triple_target, triples, N).wait();

    upcxx::barrier();

    for (int i = 0; i < N; i++) {
        assert(recv_buff.local()[i] == prev);
        assert(triple_recv_buff.local()[i].x == prev);
        assert(triple_recv_buff.local()[i].y == prev);
        assert(triple_recv_buff.local()[i].z == prev);
    }

    upcxx::barrier();

    if (me == 0) {
        std::cout << "SUCCESS" << std::endl;
    }

    upcxx::finalize();

    return 0;
}
