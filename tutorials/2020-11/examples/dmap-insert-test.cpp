// Tests the insert operation on the distributed hash table.

#include <iostream>
#include <random>
#include "dmap.hpp"

using namespace std;
using namespace upcxx;

int main() {
  init();
  const long N = 1000;
  DistrMap dmap;

  // initialize key and value for first insertion
  string key = to_string(rank_me()) + ":" + to_string(0);
  string val = key;
  for (long i = 0; i < N; i++) {
    future<> fut = dmap.insert(key, val);
    // compute new key while waiting for RPC to complete
    if (i < N - 1) {
      key = to_string(rank_me()) + ":" + to_string(i + 1);
      val = key;
    }
    // wait for operation to complete before next insert
    fut.wait();
  }

  // barrier to ensure all insertions have completed
  barrier();
  for (long i = 0; i < N; i++) {
    string key = to_string((rank_me() + 1) % rank_n()) + ":" +
      to_string(i);
    string val = dmap.find(key).wait();
    // attach callback, which itself returns a future
    future<> fut = dmap.find(key).then(
      // lambda to check the return value
      [key](string val) {
        assert(val == key);
      });
    // wait for future and its callback to complete
    fut.wait();
  }

  barrier(); // wait for finds to complete globally
  if (!rank_me()) cout << "SUCCESS" << endl;
  finalize();
}
