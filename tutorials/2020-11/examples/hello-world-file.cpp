// Hello world example with writes to a file.
// Each process independently writes to the file, with no
// synchronization. The writes are race conditions, and the results
// are undefined.

#include <iostream>
#include <fstream>
#include <unistd.h> // for sync
#include <upcxx/upcxx.hpp>

// we will assume this is always used in all examples
using namespace std;

int main() {
  // setup UPC++ runtime
  upcxx::init();
  // open a file for writing
  ofstream fout("output.txt", ios_base::app);
  fout << "Hello from process " << upcxx::rank_me()
       << " out of " << upcxx::rank_n() << endl;
  // commit data to disk
  sync();
  // close down UPC++ runtime
  upcxx::finalize();
  // file will close due to RAII
}
