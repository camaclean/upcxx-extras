// Distributed hash table with insert, find, erase, and update
// operations.
// Represented as a distributed object of std::unordered_map's.

#include <map>
#include <upcxx/upcxx.hpp>

class DistrMap {
private:
  // store the local unordered map in a distributed object to access
  // from RPCs
  using dobj_map_t =
    upcxx::dist_object<std::unordered_map<std::string, std::string>>;

  dobj_map_t local_map{{}};

  // map the key to a target process
  int get_target_rank(const std::string &key) {
    return std::hash<std::string>{}(key) % upcxx::rank_n();
  }

public:
  // implicit default constructor must be called collectively, since
  // it invokes the collective dist_object constructor

  // insert a key-value pair into the hash table
  upcxx::future<> insert(const std::string &key,
                         const std::string &val) {
    return upcxx::rpc(get_target_rank(key),
                      // lambda to insert the key-value pair
                      [](dobj_map_t &lmap, const std::string &key,
                         const std::string &val) {
                        // insert into the local map at the target
                        (*lmap)[key] = val;
                      }, local_map, key, val);
  }

  // find a key and return associated value in a future
  upcxx::future<std::string> find(const std::string &key) {
    return upcxx::rpc(get_target_rank(key),
                      // lambda to find the key in the local map at
                      // the target
                      [](dobj_map_t &lmap, const std::string &key) {
                        if (lmap->count(key) == 0) {
                          // no key found
                          return std::string("NOT FOUND");
                        }
                        // the key was found, return the value
                        return (*lmap)[key];
                      }, local_map, key);
  }

  //======================================================================
  //  EXERCISE:
  //
  // Implement the erase method, to remove a key from the map
  //   return future<>, which signals completion of the operation
  upcxx::future<> erase(const std::string &key) {
    return upcxx::rpc(get_target_rank(key),
                      // lambda to remove the key from the local map
                      // at the target
                      [](dobj_map_t &lmap, const std::string &key) {
                        // FILL IN CODE HERE
                        //
                        // HINT: you'll need to call erase() on an
                        //       unordered_map
                        lmap->erase(key);
                      }, local_map, key);
  }

  //======================================================================
  //  EXERCISE:
  //
  // Implement the update method, to insert a new value in the map
  // whether or not the key is already in the map
  //   return future<str>, where str is the old value if it exists, or
  //   "NOT FOUND" if it does not
  upcxx::future<std::string> update(const std::string &key,
                                    const std::string &value) {
    int target = get_target_rank(key);
    // FILL IN CODE HERE
    //
    // HINT: you'll need to invoke an RPC to target and call
    //       local_update() on an unordered_map from within the RPC
    //       function; the RPC function should take in a distributed
    //       object by reference and two strings by value
    return upcxx::rpc(target,
                      // lambda to update the key in the local map at
                      // the target
                      [](dobj_map_t &lmap, const std::string &key,
                         const std::string &value) {
                        return local_update(*lmap, key, value);
                      }, local_map, key, value);
  }

  // perform an update on a std::unordered_map that resides on the
  // local process
  static std::string local_update(std::unordered_map<std::string,
                                                     std::string> &lmap,
                                  const std::string &key,
                                  const std::string &value) {
    std::string old_value;
    if (lmap.count(key) == 0) {
      // no key found
      old_value = "NOT FOUND";
    } else {
      // the key was found, save the old value
      old_value = lmap[key];
    }
    // insert the new key/value pair
    lmap[key] = value;
    return old_value;
  }
};
