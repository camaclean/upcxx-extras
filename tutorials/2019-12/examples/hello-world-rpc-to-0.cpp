// First hello world example.
// Each process independently says hello, with no synchronization.

#include <iostream>
#include <upcxx/upcxx.hpp>

// we will assume this is always used in all examples
using namespace std;

int main() {
  // setup UPC++ runtime
  upcxx::init();
  for (int i = 0; i < upcxx::rank_n(); ++i) {
    if (upcxx::rank_me() == i) {
      // send RPC to 0 when it is my turn
      upcxx::rpc(0, [](int rank) {
        cout << "Hello from process " << rank << endl;
      }, upcxx::rank_me()).wait();
    }
    // barrier prevents anyone from proceeding until everyone is here
    upcxx::barrier();
  }
  // close down UPC++ runtime
  upcxx::finalize();
} 
