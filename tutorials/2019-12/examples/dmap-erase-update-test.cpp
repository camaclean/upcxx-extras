// Tests the insert operation on the distributed hash table.

#include <iostream>
#include <random>
#include "dmap-erase-update.hpp"

using namespace std;
using namespace upcxx;

int main() {
  init();
  const long N = 1000;
  DistrMap dmap;

  // initialize key and value for first insertion
  string key = to_string(rank_me()) + ":" + to_string(0);
  string val = key;
  for (long i = 0; i < N; i++) {
    future<> fut = dmap.insert(key, val);
    // compute new key while waiting for RPC to complete
    if (i < N - 1) {
      key = to_string(rank_me()) + ":" + to_string(i + 1);
      val = key;
    }
    // wait for operation to complete before next insert
    fut.wait();
  }

  // erase the first N/2 keys
  key = to_string(rank_me()) + ":" + to_string(0);
  for (long i = 0; i < N / 2; i++) {
    future<> fut = dmap.erase(key);
    if (i < N / 2 - 1) {
      key = to_string(rank_me()) + ":" + to_string(i + 1);
      val = key;
    }
    // wait for operation to complete before next insert
    fut.wait();
  }

  // update the second N/2 keys and test the old value
  key = to_string(rank_me()) + ":" + to_string(N / 2);
  val = key + "u";
  string prev_key = key;
  for (long i = N / 2; i < N; i++) {
    future<string> fut = dmap.update(key, val);
    prev_key = key; // save previous key for check
    if (i < N - 1) {
      key = to_string(rank_me()) + ":" + to_string(i + 1);
      val = key + "u";
    }
    // check result of update before next insert
    fut.wait();
    assert(fut.result() == prev_key);
  }

  // barrier to ensure all insertions have completed
  barrier();
  for (long i = 0; i < N; i++) {
    string key = to_string((rank_me() + 1) % rank_n()) + ":" +
      to_string(i);
    string val = dmap.find(key).wait();
    // attach callback, which itself returns a future
    future<> fut = dmap.find(key).then(
      // lambda to check the return value
      [key,i](string val) {
        if (i < N / 2) {
          assert(val == "NOT FOUND"); // erased
        } else {
          assert(val == key + "u"); // updated
        }
      });
    // wait for future and its callback to complete
    fut.wait();
  }

  barrier(); // wait for everything to complete globally
  if (!rank_me()) cout << "SUCCESS" << endl;
  finalize();
}
