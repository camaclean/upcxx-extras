// Distributed hash table with insert and find operations.
// Represented as a distributed object of std::unordered_map's.

#include <map>
#include <upcxx/upcxx.hpp>

class DistrMap {
private:
  // store the local unordered map in a distributed object to access
  // from RPCs
  using dobj_map_t =
    upcxx::dist_object<std::unordered_map<std::string, std::string>>;

  dobj_map_t local_map{{}};

  // map the key to a target process
  int get_target_rank(const std::string &key) {
    return std::hash<std::string>{}(key) % upcxx::rank_n();
  }

public:
  // implicit default constructor must be called collectively, since
  // it invokes the collective dist_object constructor

  // insert a key-value pair into the hash table
  upcxx::future<> insert(const std::string &key,
                         const std::string &val) {
    return upcxx::rpc(get_target_rank(key),
                      // lambda to insert the key-value pair
                      [](dobj_map_t &lmap, std::string key,
                         std::string val) {
                        // insert into the local map at the target
                        lmap->insert({key, val});
                      }, local_map, key, val);
  }

  // find a key and return associated value in a future
  upcxx::future<std::string> find(const std::string &key) {
    return upcxx::rpc(get_target_rank(key),
                      // lambda to find the key in the local map at
                      // the target
                      [](dobj_map_t &lmap, std::string key) {
                        auto elem = lmap->find(key);
                        if (elem == lmap->end()) {
                          // no key found
                          return std::string("NOT FOUND");
                        }
                        // the key was found, return the value
                        return elem->second;
                      }, local_map, key);
  }
};
