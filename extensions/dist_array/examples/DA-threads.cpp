#include <atomic>
#include <random>
#include <thread>
#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

std::atomic<int> count(0);

void loop(extras::dist_array<double,0,true> *dist, std::vector<size_t> *vec, 
        size_t i, size_t iters) {
    future<> fut_all = make_future();
    for (size_t j = 0; j < iters; j++) {
        size_t idx = vec->at(i*iters+j);
        future<> fut = dist->ptr(idx)
            .then([&dist,idx](global_ptr<double> gptr) { 
                 assert(gptr); 
                 assert(gptr.where() == dist->global_idx_info(idx).rank());
            });
        fut_all = when_all(fut_all, fut);
        if (!(j%10)) progress();
    }
    fut_all.wait();
    count++;
}

int main(int argc, char *argv[]) {
    size_t threads = 4, block = 24;
    if (argc > 1) {
        if (argc > 2) {
            if (argc > 3) {
                std::cerr << "Usage: upcxx-run -n [RANKS] ./DA-threads "
                    "[numthreads] [blocksize]" << std::endl;
                std::terminate();
            }
            block = atol(argv[2]);
        }
        threads = atol(argv[1]);
    }
    init();
    if (rank_n()==1)
        std::cout << "DA-threads should be run with multiple ranks. "
            "Cache will be filled on construction." << std::endl;
    extras::dist_array<double,0,true> AD(block*rank_n(), extras::PURE_BLOCKED, 
            world(), extras::dist_array<double,0,true>::params(std::max(1,rank_n()/2)));
    extras::dist_array<double,0,true> DA(std::move(AD)); 
    for (auto &elt : DA.process_view()) 
        elt = 1.23;
    std::default_random_engine gen;
    std::uniform_int_distribution<size_t> idist(0,DA.global_size()-1);
    std::vector<size_t> idxs(DA.global_size());
    size_t chunk = idxs.size()/threads; // if idxs.size()%n != 0 then not all indices get used
    for (auto &elt : idxs)
        elt = idist(gen); // each rank will look for same global indices
    std::vector<std::thread> workers;
    for (size_t i = 0; i < threads; i++)
        workers.push_back(std::thread(loop, &DA, &idxs, i, chunk));
    while(count<(int)threads) 
        progress();
    for (auto & th : workers) th.join();
    barrier();
    if (!rank_me()) std::cout << "SUCCESS" << std::endl;
    finalize();
}
