#include <fstream>
#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

int main(int argc, char *argv[]) {
    size_t BS=0;
    if (argc>1) {
        BS = atol(argv[1]);
        if (argc>2) {
            std::cerr << "Usage: upcxx-run -n [RANKS] ./DA-scatter [block size]" << std::endl;
            std::terminate();
        }
    }
    init();
    constexpr size_t N = 1000;
    extras::dist_array<double> my_dist_array(N,BS);
    barrier();
    if (!rank_me()) {
        std::ifstream input("examples/input");
        static double data_to_distribute[N];
        for (size_t i = 0; i < N; i++)
            input >> data_to_distribute[i];
        if (!BS)
            BS = my_dist_array.block_size();
        size_t block_id = 0;
        promise<> p;
        std::for_each(my_dist_array.bbegin(), my_dist_array.bend(),  // block iterators
                    [BS,&p,&block_id](future<global_ptr<double>> f_gptr) {
                      f_gptr.then([BS,&p,block_id](global_ptr<double> gptr) { // write to each block
                         rput(&data_to_distribute[BS*block_id], 
                              gptr, BS, operation_cx::as_promise(p));
                      });
                      block_id++;
                    });
        p.finalize().wait();
        block_id = 0;
        bool passed = true;
        double *buffer = new double[BS];
        for (auto it = my_dist_array.bbegin(); it < my_dist_array.bend(); it++) {
            rget(it->wait(), buffer, BS).wait();
            for (size_t i = 0; i < BS; i++) {
                if (data_to_distribute[block_id*BS+i] != buffer[i]) {
                    passed = false;
                    std::cout << "ERROR: scattered data doesn't match " 
                        "expected value" << std::endl;
                    break;       
                }            
            }
            block_id++;
        }
        delete [] buffer;
        if (passed)
            std::cout << "SUCCESS" << std::endl;
    }
    barrier();
    finalize();
}
