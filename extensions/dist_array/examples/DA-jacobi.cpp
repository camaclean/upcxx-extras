#include <random>
#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

int main(int argc, char *argv[]) {
    long N = 128, iters = 10;
    if (argc > 1) {
        std::cerr << "Usage: upcxx-run -n [RANKS] ./DA-jacobi" << std::endl;
        std::terminate();
    }
    init();
    // construct data arrays, default to pure blocked
    extras::dist_array<double> old_DA(N), new_DA(N);
    // find global index of where (only) local slice begins
    size_t offset = old_DA.process_idx_info(0).global_idx();
    size_t size_me = old_DA.process_size();
    assert(size_me >= 2); // assume N >= ranks*2
    size_t left = (offset-1+N)%N, right = (offset+size_me)%N;
    global_ptr<double> old_left =  old_DA.ptr(left).wait();
    global_ptr<double> new_left =  new_DA.ptr(left).wait();
    global_ptr<double> old_right = old_DA.ptr(right).wait();
    global_ptr<double> new_right = new_DA.ptr(right).wait();
    std::mt19937_64 rgen(1); rgen.discard(offset);
    for (auto &e : old_DA.process_view()) { e = rgen()%100; }
    double *old_grid = old_DA.data();
    double *new_grid = new_DA.data();
    for (long it = 0; it < iters; it++) {
        barrier();

        // start fetch of ghost cells
        future<double> left_ghost = rget(old_left);
        future<double> right_ghost = rget(old_right);

        // compute interior cells
        for (long i = 1; i < (long)size_me-1; i++)
            new_grid[i] = 0.25 * (old_grid[i-1] + 2*old_grid[i] + old_grid[i+1]);

        // finish comms and compute boundary cells
        new_grid[0] = 0.25 * (left_ghost.wait() + 2*old_grid[0] + old_grid[1]);
        new_grid[size_me-1] = 0.25 * (old_grid[size_me-2] + 2*old_grid[size_me-1]
                                                          + right_ghost.wait());
        // swap grids for next iteration
        std::swap(old_grid,new_grid);
        std::swap(old_left,new_left);
        std::swap(old_right,new_right);
    }
    double sum = 0;
    for (auto &e : old_DA.process_view()) { sum += e; }
    sum = reduce_all(sum, op_fast_add).wait();

    // check known results for global_domain_sz x iters
    static struct { long sz, iters; double correct; } known_answers[] = {
                {    8192,   100,       408097 },
                {    4096,   100,       207466 },
                {    2048,   100,       103965 },
                {    1024,   100,        51900 },
                {     512,   100,        26021 },
                {     256,   100,        12596 },
                {     128,   100,         6129 },
                {      64,   100,         2943 },
                {      32,   100,         1502 },
                {      16,   100,          760 },
                {       8,   100,          352 },
                {       4,   100,          166 },
    };

    const char *result = "(unknown problem size)";
    if (!rank_me()) {
        for (auto &answer : known_answers) {
            if (N == answer.sz
                /* && iters == answer.iters */ // iteration count does not mathematically affect sum
                ) {
              if (fabs(sum - answer.correct) < 0.001)
                   result = "SUCCESS";
              else { result = "ERROR: jacobi sumcheck failed"; }
              break;
            }
        }
    }
    barrier();
    if (!rank_me()) std::cout << result << std::endl;
    finalize();
}
