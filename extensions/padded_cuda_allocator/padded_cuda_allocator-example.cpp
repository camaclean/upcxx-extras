#include <iostream>
#include <cassert>

#include <upcxx/upcxx.hpp>
#include "upcxx-extras/padded_cuda_allocator.hpp"

using namespace std;
using namespace upcxx;
using namespace upcxx::extras;

int main() {
    upcxx::init();

    auto gpu_device = upcxx::cuda_device( 0 );

    // alloc GPU segment
    auto padded_gpu_alloc = extras::padded_cuda_allocator(gpu_device,
            1024 * 1024);

    size_t width = 10;
    size_t height = 10;
    size_t pitch;
    global_ptr<double, memory_kind::cuda_device> dptr =
        padded_gpu_alloc.allocate_pitched<double>(width, height, pitch);
    assert(dptr);

    std::cout << "Allocated device pointer with pitch " << pitch << std::endl;
    /*
     * Assert some loose but reasonable assumptions about pitch, given that most
     * CUDA GPUs have at least 128-byte memory transactions.
     */
    assert(pitch >= 128 && pitch % 128 == 0);

    // Show we are still able to use the flat CUDA allocation routines
    global_ptr<double, memory_kind::cuda_device> dptr2 =
        padded_gpu_alloc.allocate<double>(width);
    assert(dptr2);

    double *h_row = (double *)malloc(width * sizeof(*h_row));
    assert(h_row);
    for (size_t i = 0; i < width; i++) {
        h_row[i] = i;
    }

    // Copy a single row from the host into the second row on the device
    cudaError_t err = cudaMemcpy(
            padded_gpu_alloc.local(index_pitched(dptr, 1UL, 0UL, pitch)),
            h_row, width * sizeof(*h_row), cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);

    err = cudaMemcpy(h_row,
            padded_gpu_alloc.local(index_pitched(dptr, 1UL, 0UL, pitch)),
            width * sizeof(*h_row), cudaMemcpyDeviceToHost);
    assert(err == cudaSuccess);

    for (size_t i = 0; i < width; i++) {
        assert(h_row[i] == i);
    }

    free(h_row);
    padded_gpu_alloc.deallocate(dptr2);
    padded_gpu_alloc.deallocate(dptr);

    gpu_device.destroy();

    // For CI
    upcxx::barrier();
    if (upcxx::rank_me() == 0) std::cout << "SUCCESS" << std::endl;

    upcxx::finalize();

    return 0;
}
