// Author: Max Grossman
#include <iostream>
#include <cuda.h>
#include <cuda_runtime_api.h>

#ifndef USE_UPCXX
  #if __CUDACC__ 
    // a bug in cudafe++ affects compilation of UPC++ headers up to 2019.9.0
    // for details, see issue 274
    #define USE_UPCXX 0
  #else 
    #define USE_UPCXX 1
  #endif
#endif

#if USE_UPCXX
#include <upcxx/upcxx.hpp>
#endif

namespace upcxx { namespace extras {

/*
 * Utility for estimating the memory usage of a pitched allocation with the
 * provided dimensions. This estimate includes only the memory usage of the
 * allocation itself: whatever bytes are necessary to allocate a width x height
 * array with each row of length width aligned to the estimated pitch. This does
 * not include any additional padding done by the UPC++ allocator. This utility
 * is useful for sizing CUDA memory segments when initializing a CUDA allocator.
 */
template <typename T>
std::size_t estimate_pitched_memory_usage(std::size_t width,
        std::size_t height, std::size_t estimated_device_pitch = 512) {
    size_t width_in_bytes = width * sizeof(T);
    size_t padding = (width_in_bytes % estimated_device_pitch == 0 ? 0 :
            estimated_device_pitch - (width_in_bytes % estimated_device_pitch));
    size_t padded_width = width_in_bytes + padding;
    return height * padded_width;
}

/*
 * Utility for computing the offset of element (row, col) in a 2D
 * pitched array with base address ptr. This variant of index_pitched is
 * appropriate for use within a CUDA kernel when directly interacting
 * with device pointers.
 */
template <typename T, typename index_type>
#ifdef __CUDA_ARCH__
__host__ __device__
#endif
inline T *index_pitched(T *ptr, index_type row, index_type col,
        index_type pitch) {
    static_assert(std::is_integral<index_type>::value && sizeof(index_type) > 1,
            "Index type to index_pitched must be integral and at least 16-bit");
    return  (T*)((char*)ptr + row * pitch) + col;
}

/*
 * Compute the offset in bytes from the start of a pitched allocation of a given
 * element specified by (row, col).
 */
template <typename T, typename index_type>
#ifdef __CUDA_ARCH__
__host__ __device__
#endif
inline index_type byte_offset_pitched(index_type row, index_type col,
        index_type pitch) {
    static_assert(std::is_integral<index_type>::value && sizeof(index_type) > 1,
            "Index type to byte_offset_pitched must be integral and at least 16-bit");
    return row * pitch + col * sizeof(T);
}

/*
 * Same as the above, but for upcxx::global_ptr.
 */
#if USE_UPCXX
template <typename T, typename index_type>
upcxx::global_ptr<T, upcxx::memory_kind::cuda_device> index_pitched(
        upcxx::global_ptr<T, upcxx::memory_kind::cuda_device> ptr,
        index_type row, index_type col, index_type pitch) {
    static_assert(std::is_integral<index_type>::value && sizeof(index_type) > 1,
            "Index type to index_pitched must be integral and at least 16-bit");
    upcxx::global_ptr<uint8_t, upcxx::memory_kind::cuda_device> cptr =
        upcxx::reinterpret_pointer_cast<uint8_t>(ptr);
    cptr = (cptr + (row * pitch));
    return upcxx::reinterpret_pointer_cast<T>(cptr) + col;
}

class padded_cuda_allocator: public device_allocator<upcxx::cuda_device> {
    private:
        size_t pitch;

    public:
        padded_cuda_allocator(upcxx::cuda_device& dev, std::size_t size) :
                device_allocator<upcxx::cuda_device>(dev, size) {
            /*
             * Do a one-element allocation to figure ou the pitch used by the
             * CUDA runtime.
             */
            void *ptr;
            cudaError_t err = cudaMallocPitch(&ptr, &pitch, 1, 2);
            UPCXX_ASSERT_ALWAYS(err == cudaSuccess);
            err = cudaFree(ptr);
            UPCXX_ASSERT_ALWAYS(err == cudaSuccess);
        }

        /*
         * Equivalent to cudaMallocPitch, but with width and height expressed in
         * terms of T rather than in terms of bytes. The value returned in
         * out_pitch is still in terms of bytes.
         */
        template<typename T>
        upcxx::global_ptr<T, upcxx::memory_kind::cuda_device> allocate_pitched(
                std::size_t width, std::size_t height, std::size_t &out_pitch) {
            size_t width_in_bytes = width * sizeof(T);
            size_t padding = (width_in_bytes % pitch == 0 ? 0 :
                    pitch - (width_in_bytes % pitch));
            size_t padded_width = width_in_bytes + padding;

            out_pitch = padded_width;

            upcxx::global_ptr<uint8_t, upcxx::memory_kind::cuda_device> mem;
#if UPCXX_SPEC_VERSION >= 20190301
            mem = this->allocate<uint8_t>(height * padded_width, pitch);
#else
            /*
             * 512 is hard-coded here as an upper bound of the natural alignment
             * of CUDA global memory. This workaround is needed for any versions
             * of UPC++ prior to the resolution of UPC++ spec issue #145.
             */
            UPCXX_ASSERT(pitch <= 512);
            mem = this->allocate<uint8_t, 512>(height * padded_width);
#endif
            return upcxx::reinterpret_pointer_cast<T>(mem);
        }
};
#endif

}} // namespace upcxx::extras

